let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

/**
 * Original Laravel JS & SCSS compiler. Project VueJS included below!
 */
mix.js('resources/assets/js/app.js', 'public/js')
   .sass('resources/assets/sass/app.scss', 'public/css');

/**
 * Admire Template Compiler
 * Faild to include | c3 - d3
 */
const dest = 'resources/assets/vendors/';


mix.styles(
    [
        dest + 'components.css',
        dest + 'custom.css',
        dest + 'layouts.css',
        dest + 'chartist/css/chartist.min.css',
        dest + 'circliful/css/jquery.circliful.css',
        dest + 'toastr/css/toastr.min.css',
        dest + 'switchery/css/switchery.min.css',

        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',

        // Skins - Choose one only
        // dest + 'admire-skins/black_skin.css',
        dest + 'admire-skins/blue_black_skin.css',
        // dest + 'admire-skins/blue_skin.css',
        // dest + 'admire-skins/brown_black_skin.css',
        // dest + 'admire-skins/brown_skin.css',
        // dest + 'admire-skins/green_black_skin.css',
        // dest + 'admire-skins/green_skin.css',
        // dest + 'admire-skins/mint_black_skin.css',
        // dest + 'admire-skins/mint_skin.css',
        // dest + 'admire-skins/orange_black_skin.css',
        // dest + 'admire-skins/orange_skin.css',
        // dest + 'admire-skins/purple_black_skin.css',
        // dest + 'admire-skins/purple_skin.css',
        // dest + 'admire-skins/red_black_skin.css',
        // dest + 'admire-skins/red_skin.css',
        // dest + 'admire-skins/skin.css',
    ],
    'public/assets/css/all-plugins.css'
);

mix.scripts(
    [
        dest + 'components.js',
        dest + 'custom.js',
        dest + 'flip/js/jquery.flip.min.js',
        dest + 'pluginjs/jquery.sparkline.js',
        dest + 'chartist/js/chartist.min.js',
        dest + 'pluginjs/chartist-tooltip.js',
        dest + 'swiper/js/swiper.min.js',
        dest + 'circliful/js/jquery.circliful.min.js',
        dest + 'slimscroll/js/jquery.slimscroll.min.js',
        dest + 'raphael/js/raphael.min.js',
        dest + 'toastr/js/toastr.min.js',
        dest + 'switchery/js/switchery.min.js',
        dest + 'flotchart/js/jquery.flot.js',
        dest + 'flotchart/js/jquery.flot.resize.js',
        dest + 'flotchart/js/jquery.flot.stack.js',
        dest + 'flotchart/js/jquery.flot.time.js',
        dest + 'flotspline/js/jquery.flot.spline.min.js',
        dest + 'flotchart/js/jquery.flot.categories.js',
        dest + 'flotchart/js/jquery.flot.pie.js',
        dest + 'flot.tooltip/js/jquery.flot.tooltip.min.js',
        dest + 'jquery.newsTicker/js/newsTicker.js',
        dest + 'countUp.js/js/countUp.min.js',

        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',
        // dest + 'addHere',

    ],
    'public/assets/js/all-plugins.js'
);


